<?php

class Ufhs_Systemselector_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();

		// Update the layout to inject system selector shizzle
		$this->getLayout()->getBlock('root')->setTemplate('page/1column.phtml');
		$this->getLayout()->getBlock('head')->addCss('systemselector/css/style.css');

		// Add the system selector template to the content section of the main layout
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'system_selector', ['template' => 'systemselector/selector.phtml']);
		$this->getLayout()->getBlock('content')->append($block);

        $this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
	}

	public function resultAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['code']) && strlen($post['code']) == 4) {
			$collection = Mage::getModel("systemselector/result")
			->getCollection()
			->addFieldToFilter('result', $post['code']);

			$products = [];
			foreach ($collection->getData() as $result) {
				$prod = Mage::getModel('systemselector/product')->load($result['product']);
				$mage = Mage::getModel('catalog/product')->load($prod->getMageId());
				// $prod->setImage(Mage::helper('catalog/image')->init($mage, 'image'));
				$prod->setImage($mage->getImageUrl());
				$prod->setUrl($mage->getProductUrl());
				$products[] = $prod;
			}

			echo Mage::app()->getLayout()
			->createBlock('systemselector/result')
			->setData('products', $products)
			->setTemplate('systemselector/result.phtml')
			->toHtml();
		}
	}
}